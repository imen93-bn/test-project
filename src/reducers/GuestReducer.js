import {
    GET_LIST_GUEST_BEGIN, GET_LIST_GUEST_SUCCESS, GET_LIST_GUEST_FAILURE,
    ADD_GUEST_BEGIN, ADD_GUEST_SUCCESS, ADD_GUEST_FAILURE,
    EDIT_GUEST_BEGIN, EDIT_GUEST_SUCCESS, EDIT_GUEST_FAILURE,
    DELETE_GUEST_BEGIN, DELETE_GUEST_SUCCESS, DELETE_GUEST_FAILURE
} from '../actions/GuestActions'

const initialState = {
    isLoadingList: false,
    isSuccessList: false,
    isFailureList: false,
    listGuests: [],
    isLoadingAdd: false,
    isSuccessAdd: false,
    isFailureAdd: false,
    isLoadingEdit: false,
    isSuccessEdit: false,
    isFailureEdit: false,
    isLoadingDelete: false,
    isSuccessDelete: false,
    isFailureDelete: false,
}

function guestReducer(state = initialState, action) {
    switch (action.type) {

        case GET_LIST_GUEST_BEGIN: {
            return {
                ...state,
                isLoadingList: true,
                isSuccessList: false,
                isFailureList: false,
                listGuests: [],
            }
        }
        case GET_LIST_GUEST_SUCCESS: {
            return {
                ...state,
                isLoadingList: false,
                isSuccessList: true,
                isFailureList: false,
                listGuests: action.list
            }
        }
        case GET_LIST_GUEST_FAILURE: {
            return {
                ...state,
                isLoadingList: true,
                isSuccessList: false,
                isFailureList: false,
                listGuests: [],
            }
        }
        case ADD_GUEST_BEGIN: {
            return {
                ...state,
                isLoadingAdd: true,
                isSuccessAdd: false,
                isFailureAdd: false,
            }
        }
        case ADD_GUEST_SUCCESS: {
            return {
                ...state,
                isLoadingAdd: false,
                isSuccessAdd: true,
                isFailureAdd: false,
            }
        }
        case ADD_GUEST_FAILURE: {
            return {
                ...state,
                isLoadingAdd: false,
                isSuccessAdd: false,
                isFailureAdd: true,
            }
        }
        case EDIT_GUEST_BEGIN: {
            return {
                ...state,
                isLoadingEdit: true,
                isSuccessEdit: false,
                isFailureEdit: false,
            }
        }
        case EDIT_GUEST_SUCCESS: {
            return {
                ...state,
                isLoadingEdit: false,
                isSuccessEdit: true,
                isFailureEdit: false,
            }
        }
        case EDIT_GUEST_FAILURE: {
            return {
                ...state,
                isLoadingEdit: false,
                isSuccessEdit: false,
                isFailureEdit: true,
            }
        }
        case DELETE_GUEST_BEGIN: {
            return {
                ...state,
                isLoadingDelete: true,
                isSuccessDelete: false,
                isFailureDelete: false,
            }
        }
        case DELETE_GUEST_SUCCESS: {
            return {
                ...state,
                isLoadingDelete: false,
                isSuccessDelete: true,
                isFailureDelete: false,
            }
        }
        case DELETE_GUEST_FAILURE: {
            return {
                ...state,
                isLoadingDelete: false,
                isSuccessDelete: false,
                isFailureDelete: true,
            }
        }
        default:
            return state
    }
}

export default guestReducer;