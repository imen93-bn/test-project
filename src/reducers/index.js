import { combineReducers } from "redux";
import loginReducer from "./LoginReducer";
import guestReducer from './GuestReducer';

export default function getReducers() {
    return combineReducers({
        loginReducer: loginReducer,
        guestReducer: guestReducer
    })
}