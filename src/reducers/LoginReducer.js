import {
    LOGIN_FAILURE, LOGIN_BEGIN, LOGIN_SUCCESS
} from '../actions/LoginActions'

const initialState = {
    isLoadingLogin: true,
    isSuccessLogin: false,
    isFailureLogin: false,
    token: ""
}
function loginReducer(state = initialState, action) {
    switch (action.type) {
        case LOGIN_BEGIN: {
            return {
                ...state,
                isLoadingLogin: true,
                isSuccessLogin: false,
                isFailureLogin: false,
            }
        }
        case LOGIN_SUCCESS: {
            return {
                ...state,
                isLoadingLogin: false,
                isSuccessLogin: true,
                isFailureLogin: false,
                token: action.token
            }
        }
        case LOGIN_FAILURE: {
            return {
                ...state,
                isLoadingLogin: false,
                isSuccessLogin: false,
                isFailureLogin: true,
            }
        }
        default:
            return state
    }
}

export default loginReducer;