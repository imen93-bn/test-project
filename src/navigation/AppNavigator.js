import React from 'react';
import { View,Platform } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
//import { isAuthenticated } from '../actions/Login';
import LoginScreen from '../screens/login';
import GuestScreen from '../screens/guest';
import DetailsGuestScreen from '../screens/guest/DetailsGuest';

const Stack = createStackNavigator();

const AppNavigator = () => {

    return (
        <View style={{ flex: 1 }}>
            <NavigationContainer >
                <Stack.Navigator initialRouteName={'Login'}>
                    <Stack.Screen
                        name="Login"
                        component={LoginScreen}
                        options={{
                            headerShown: false,
                        }}
                    />
                    <Stack.Screen
                        name="Guest"
                        component={GuestScreen}
                        options={({ route, navigation }) => ({
                            headerStyle: {
                                backgroundColor: "#248D9A",
                                borderBottomWidth: 0,
                                justifyContent: "center",
                                alignItems: "center",
                                // Android
                                elevation: 0,
                                // iOS
                                shadowColor: "black",
                                shadowOpacity: 0,
                                shadowOffset: {
                                    height: 0,
                                },
                                shadowRadius: 0
                            },
                            headerTitleStyle: {
                                fontSize: 16,
                                color: "white",
                                fontWeight: Platform.OS === 'ios' ? null : "200"
                            }
                            
                        })}
                    />
                    <Stack.Screen
                        name="DetailsGuest"
                        component={DetailsGuestScreen}
                        options={({ route, navigation }) => ({
                            headerStyle: {
                                backgroundColor: "#248D9A",
                                borderBottomWidth: 0,
                                justifyContent: "center",
                                alignItems: "center",
                                // Android
                                elevation: 0,
                                // iOS
                                shadowColor: "black",
                                shadowOpacity: 0,
                                shadowOffset: {
                                    height: 0,
                                },
                                shadowRadius: 0
                            },
                            headerTitleStyle: {
                                fontSize: 16,
                                color: "white",
                                fontWeight: Platform.OS === 'ios' ? null : "200"
                            }
                            
                        })}
                    />
                </Stack.Navigator>
            </NavigationContainer>
        </View>
    );
};

export default AppNavigator;


