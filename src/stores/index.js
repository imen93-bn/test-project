import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import ReduxThunk from "redux-thunk";
import getReducers from "../reducers";

const sagaMiddleware = createSagaMiddleware();

const middleWares = [sagaMiddleware, ReduxThunk]

export const store = createStore(
  getReducers(),
  applyMiddleware(...middleWares)
)