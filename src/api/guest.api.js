import axios from 'axios';
import BASE_URL from '../utils/Constants';


export const getGuests = (token) => {
    return axios({
        method: 'GET',
        url: `${BASE_URL}/guests`,
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        },
    });
};


export const addGuest = (token, body) => {
    return axios({
        method: 'POST',
        url: `${BASE_URL}/guests`,
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        },
        data: JSON.stringify(body),
    });
};

export const editGuest = (token, body, id) => {
    return axios({
        method: 'PUT',
        url: `${BASE_URL}/guests/${id}`,
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        },
        data: JSON.stringify(body),
    });
};


export const deleteGuest = (token, id) => {
    return axios({
        method: 'DELETE',
        url: `${BASE_URL}/guests/${id}`,
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        }
    });
};