import axios from 'axios';
import BASE_URL from '../utils/Constants';


export const login = (body) => {
    return axios({
        method: 'POST',
        url: `${BASE_URL}/login`,
        headers: {
            'Content-Type': 'application/json',
        },
        data: JSON.stringify(body),
    });
};