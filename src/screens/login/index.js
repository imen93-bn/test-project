import React, { Component } from "react";
import { View, TextInput, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { connect } from "react-redux";
import { LogIn } from '../../actions/LoginActions';

class LoginScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: ""
        }
    }

    // Update state username
    handleChangeUsername = (text) => {
        this.setState({ username: text })
    }
    // Update state password
    handleChangePassword = (text) => {
        this.setState({ password: text })
    }

    // On press button connexion
    onPressLogin = () => {
        let data = {
                "username": this.state.username,
                "password": this.state.password
        }
        this.props.LogIn(data)
            .then(() => {
                if (this.props.isSuccessLogin) {
                    this.props.navigation.navigate("Guest")
                }
            })
            .catch((err) => {

            })

    }
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.txtWelcome}>{"Bienvenue"}</Text>
                <View style={styles.subContainer}>
                    <TextInput
                        style={styles.input}
                        placeholder={"Nom d'utilisateur"}
                        value={this.state.username}
                        onChangeText={(text) => this.handleChangeUsername(text)}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder={"Mot de passe"}
                        value={this.state.password}
                        onChangeText={(text) => this.handleChangePassword(text)}
                    />
                    <View>
                        <TouchableOpacity style={styles.loginButton} onPress={() => this.onPressLogin()} >
                            <Text style={styles.loginButtonText}>{"Connexion"}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
};

// Définition des arguments que le composant a besoin
const mapToState = state => {
    return {
        isSuccessLogin: state.loginReducer.isSuccessLogin,
        isFailureLogin: state.loginReducer.isFailureLogin,
    };
};
export default connect(mapToState, { LogIn })(LoginScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    subContainer: {
        marginTop: 38,
        width: "90%"
    },
    txtWelcome: {
        textAlign: "center",
        fontSize: 28,
        color: "#248D9A"
    },
    loginButtonText: {
        fontSize: 18,
        color: "white"
    },
    loginButton: {
        marginTop: 38,
        borderRadius: 48 / 2,
        height: 48,
        backgroundColor: "#248D9A",
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        marginTop: 16,
        fontSize: 14,
        height: 42,
        backgroundColor: "#ECEFF1",
        padding: 8
    }
});