import React, { Component } from "react";
import { View, Text, StyleSheet, TextInput, TouchableOpacity, Alert,Image } from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import { connect } from "react-redux";
import { addNewGuest, updateGuest } from '../../actions/GuestActions';

class DetailsGuestScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstname: "",
            lastname: "",
            actionType: "",
            willCome: false,
            guest: null
        }
    }
    componentDidMount() {
        const { type, guest } = this.props.route.params;
        this.props.navigation.setOptions({
            title: type == "ADD" ? "Ajouter un invité" : "Modifier un invité",
            headerLeft: (props) => (
                <TouchableOpacity
                style={[styles.button, this.props.buttonStyle]}
                onPress={()=>this.props.navigation.goBack()}>
                    <Image
                             source={require('../../assets/back.png')}
                            resizeMode={'contain'}
                            style={styles.icon} />
            </TouchableOpacity>
            )
        });
        if (guest) {
            this.setState({
                firstname: guest.firstname,
                lastname: guest.lastname,
                willCome: guest.willCome,
                actionType: type,
                guest: guest
            })
        }
    }
    // Update state firstname
    handleChangeFirstName = (text) => {
        this.setState({ firstname: text })
    }
    // Update state lastname
    handleChangeLastName = (text) => {
        this.setState({ lastname: text })
    }
    // Add new guest
    onPressAdd = () => {
        let data = {
            "firstname": this.state.firstname,
            "lastname": this.state.lastname,
            "willCome": this.state.willCome
        }
        this.props.addNewGuest(this.props.token, data)
            .then(() => {
                if (this.props.isSuccessAdd) {
                    Alert.alert("Ajout avec succée");
                    this.props.navigation.goBack();
                }
            }).catch(() => { })
    }
    // Edit guest
    onPressEdit = () => {
        let data = {
            "firstname": this.state.firstname,
            "lastname": this.state.lastname,
            "willCome": this.state.willCome
        }
        this.props.updateGuest(this.props.token, data, this.state.guest.id)
            .then(() => {
                if (this.props.isSuccessEdit) {
                    Alert.alert("Édition avec succée");
                }
            })
            .catch(() => { })
    }
    // Update state willcome
    onValueChange = (val) => {
        this.setState({
            willCome: val
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.subContainer}>
                    <TextInput
                        style={styles.input}
                        placeholder={"Firstname"}
                        value={this.state.firstname}
                        onChangeText={(text) => this.handleChangeFirstName(text)}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder={"Lastname"}
                        value={this.state.lastname}
                        onChangeText={(text) => this.handleChangeLastName(text)}
                    />
                    {this.state.actionType == "EDIT" && <View style={styles.viewCheckBox}>
                        <Text>WillCome: </Text>
                        <CheckBox
                            value={this.state.willCome}
                            disabled={false}
                            onValueChange={(val) => this.onValueChange(val)}
                        />
                    </View>
                    }
                    <View>
                        <TouchableOpacity style={styles.loginButton} onPress={() => { this.state.actionType == "EDIT" ? this.onPressEdit() : this.onPressAdd() }} >
                            <Text style={styles.loginButtonText}>{this.state.actionType == "EDIT" ? "Modifier" : "Ajouter"}</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
        );
    }
}

// Définition des arguments que le composant a besoin
const mapToState = state => {
    return {
        token: state.loginReducer.token,
        isSuccessAdd: state.guestReducer.isSuccessAdd,
        isSuccessEdit: state.guestReducer.isSuccessEdit
    };
};
export default connect(mapToState, { addNewGuest, updateGuest })(DetailsGuestScreen);
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    subContainer: {
        flex: 1,
        marginTop: 32,
        margin: 16
    },
    loginButtonText: {
        fontSize: 18,
        color: "white"
    },
    loginButton: {
        marginTop: 38,
        borderRadius: 48 / 2,
        height: 48,
        backgroundColor: "#248D9A",
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        marginTop: 16,
        fontSize: 14,
        height: 42,
        backgroundColor: "#ECEFF1",
        padding: 8
    },
    viewCheckBox: {
        flexDirection: "row",
        marginTop: 16,
        alignItems: "center"
    },
    icon:{
        tintColor:"white",
        height: 20,
        width: 20
    }
})