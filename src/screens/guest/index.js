import React, { Component } from "react";
import { View, Text, StyleSheet, FlatList, TouchableOpacity, Image , Alert } from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import { connect } from "react-redux";
import { getListGuest, dropGuest } from '../../actions/GuestActions';


class GuestScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            datas: [],
        }
    }

    componentDidMount() {
        this.props.navigation.setOptions({
            title: "Liste des invités",
            headerLeft: (props) => (
                <TouchableOpacity
                style={[styles.button, this.props.buttonStyle]}
                onPress={()=>this.props.navigation.goBack()}>
                    <Image
                             source={require('../../assets/back.png')}
                            resizeMode={'contain'}
                            style={styles.icon} />
            </TouchableOpacity>
            )
        });
        this.getGuests();
    }

    // Get list of guests
    getGuests = () => {
        this.props.getListGuest(this.props.token)
            .then(() => {
                if (this.props.isSuccessList) {
                    this.setState({ datas: this.props.listGuests })
                }
            })
            .catch((err) => {

            })
    }

    // On press delete guest
    onPressDelete = (id) => {
        this.props.dropGuest(this.props.token, id)
            .then(() => {
                if (this.props.isSuccessDelete) {
                    Alert.alert("Suppression avec succée");
                    this.getGuests();
                }
            })
            .catch((err) => {

            })
    }

    // Return item of list
    _renderItem = ({ item }) => {
        return (
            <View>
                <View style={styles.itemContainer}>
                    <Text style={styles.txt}>{item.id}</Text>
                    <TouchableOpacity style={styles.touchName} onPress={() => this.props.navigation.navigate("DetailsGuest", { type: "EDIT", guest: item })}>
                        <Text style={styles.txt}>{item.firstname}</Text>
                        <Text style={styles.txt}>{item.lastname}</Text>
                    </TouchableOpacity>
                    <CheckBox
                        value={item.willCome}
                        disabled={false}
                    />
                    <TouchableOpacity style={styles.deleteButton} onPress={() => this.onPressDelete(item.id)} >
                        <Text style={styles.deleteButtonText}>{"Supprimer"}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    // On refresh list guests
    _onRefreshList = () => {
        this.getGuests();
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.subContainer}>
                    <FlatList
                        contentContainerStyle={styles.flatListContainer}
                        data={this.state.datas}
                        renderItem={this._renderItem}
                        extraData={this.props}
                        refreshing={false}
                        onRefresh={() => this._onRefreshList()}
                    />

                    <View style={styles.containerFabButton}>
                        <TouchableOpacity
                            style={[styles.fabButton]}
                            onPress={() => { this.props.navigation.navigate("DetailsGuest", { type: "ADD", guest: null }) }}>
                            <View>
                                <Text style={styles.plus}>+</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
        );
    }
};

// Définition des arguments que le composant a besoin
const mapToState = state => {
    return {
        token: state.loginReducer.token,
        isSuccessList: state.guestReducer.isSuccessList,
        isFailureList: state.guestReducer.isFailureList,
        isSuccessDelete: state.guestReducer.isSuccessDelete,
        listGuests: state.guestReducer.listGuests
    };
};
export default connect(mapToState, { getListGuest, dropGuest })(GuestScreen);
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    subContainer: {
        flex: 1,
        marginTop: 32,
        margin: 16
    },
    itemContainer: {
        flexDirection: "row",
        alignItems: "center",
        margin: 4
    },
    flatListContainer: {
        flexGrow: 1,
    },
    txt: {
        fontSize: 14,
    },
    touchName: {
        flex: 1,
        marginLeft: 12,
    },
    deleteButton: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 16
    },
    deleteButtonText: {
        fontSize: 12,
        height: 32,
        color: "white",
        backgroundColor: "red",
        padding: 6
    },
    containerFabButton: {
        position: 'absolute',
        right: 16,
        bottom: 76,
        //IOS
        shadowColor: "black",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.4,
        shadowRadius: 2,
        zIndex: 3
    },
    fabButton: {
        width: 56,
        height: 56,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#248D9A",
        borderRadius: 30,
        // Android 
        elevation: 8,
    },
    plus: {
        fontSize: 24,
        color: "white"
    },
    icon:{
        tintColor:"white",
        height: 20,
        width: 20
    }
});