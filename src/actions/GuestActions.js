/* 
 * GuestActions.js
 * 
 * Représente la liste des actions lié à un invité
 */
import { getGuests, addGuest, editGuest, deleteGuest } from "../api/guest.api";

export const GET_LIST_GUEST_BEGIN = 'GET_LIST_GUEST_BEGIN';
export const GET_LIST_GUEST_SUCCESS = 'GET_LIST_GUEST_SUCCESS';
export const GET_LIST_GUEST_FAILURE = 'GET_LIST_GUEST_FAILURE';

export const ADD_GUEST_BEGIN = 'ADD_GUEST_BEGIN';
export const ADD_GUEST_SUCCESS = 'ADD_GUEST_SUCCESS';
export const ADD_GUEST_FAILURE = 'ADD_GUEST_FAILURE';

export const EDIT_GUEST_BEGIN = 'EDIT_GUEST_BEGIN';
export const EDIT_GUEST_SUCCESS = 'EDIT_GUEST_SUCCESS';
export const EDIT_GUEST_FAILURE = 'EDIT_GUEST_FAILURE';

export const DELETE_GUEST_BEGIN = 'DELETE_GUEST_BEGIN';
export const DELETE_GUEST_SUCCESS = 'DELETE_GUEST_SUCCESS';
export const DELETE_GUEST_FAILURE = 'DELETE_GUEST_FAILURE';


//#Begin Region
const fetchListGuestBegin = () => ({
    type: GET_LIST_GUEST_BEGIN
});

const fetchListGuestSucces = (list) => ({
    type: GET_LIST_GUEST_SUCCESS,
    list: list
});

const fetchListGuestFailure = (error) => ({
    type: GET_LIST_GUEST_FAILURE,
    error: error,
});
//#End Region

//#Begin Region
const fetchAddGuestBegin = () => ({
    type: ADD_GUEST_BEGIN
});

const fetchAddGuestSucces = () => ({
    type: ADD_GUEST_SUCCESS
});

const fetchAddGuestFailure = (error) => ({
    type: ADD_GUEST_FAILURE,
    error: error,
});
//#End Region


//#Begin Region
const fetchEditGuestBegin = () => ({
    type: EDIT_GUEST_BEGIN
});

const fetchEditGuestSucces = () => ({
    type: EDIT_GUEST_SUCCESS
});

const fetchEditGuestFailure = (error) => ({
    type: EDIT_GUEST_FAILURE,
    error: error,
});
//#End Region

//#Begin Region
const fetchDeleteGuestBegin = () => ({
    type: DELETE_GUEST_BEGIN
});

const fetchDeleteGuestSucces = () => ({
    type: DELETE_GUEST_SUCCESS
});

const fetchDeleteGuestFailure = (error) => ({
    type: DELETE_GUEST_FAILURE,
    error: error,
});
//#End Region

// get list guests
export function getListGuest(token) {
    return async function (dispatch, getState) {
        dispatch(fetchListGuestBegin());
        return getGuests(token)
            .then(async ({ data }) => {
                let list = data["hydra:member"];
                dispatch(fetchListGuestSucces(list));
            })
            .catch(err => {
                dispatch(fetchListGuestFailure(err));
            });

    }
}

// add guest
export function addNewGuest(token, data) {
    return async function (dispatch, getState) {
        dispatch(fetchAddGuestBegin());
        return addGuest(token, data)
            .then(async ({ }) => {
                dispatch(fetchAddGuestSucces());
            })
            .catch(err => {
                dispatch(fetchAddGuestFailure(err));
            });

    }
}

// edit guest
export function updateGuest(token, data, id) {
    return async function (dispatch, getState) {
        dispatch(fetchEditGuestBegin());
        return editGuest(token, data, id)
            .then(async ({ }) => {
                dispatch(fetchEditGuestSucces());
            })
            .catch(err => {
                dispatch(fetchEditGuestFailure(err));
            });

    }
}

// delete guest
export function dropGuest(token, id) {
    return async function (dispatch, getState) {
        dispatch(fetchDeleteGuestBegin());
        return deleteGuest(token, id)
            .then(async ({ }) => {
                dispatch(fetchDeleteGuestSucces());
            })
            .catch(err => {
                dispatch(fetchDeleteGuestFailure(err));
            });

    }
}