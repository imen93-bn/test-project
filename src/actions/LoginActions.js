/* 
 * LoginActions.js
 * 
 * Représente la liste des actions de l'authentification
 */
import { login } from "../api/login.api";

export const LOGIN_BEGIN = 'LOGIN_BEGIN';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';


//#Begin Region
const fetchLoginBegin = () => ({
    type: LOGIN_BEGIN
});

const fetchLoginSucces = (token) => ({
    type: LOGIN_SUCCESS,
    token: token
});

const fetchLoginFailure = (error) => ({
    type: LOGIN_FAILURE,
    error: error,
});
//#End Region


export function LogIn(body) {
    return async function (dispatch, getState) {
        dispatch(fetchLoginBegin());
        return login(body)
            .then(async ({ data }) => {
                dispatch(fetchLoginSucces(data.token));
            })
            .catch(err => {
                dispatch(fetchLoginFailure(err));
            });

    }
}