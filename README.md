# test-project


## Installation de l'application

Clonez le projet puis exécutez la commande: **npm install**


**Android**

- Modifiez le fichier local.properties avec l'emplacement du votre SDK
- Exécutez la commande react-native run-android

**iOS**

- Naviguez vers le dossier ios dans le projet puis exécutez la commande **pod install**
- Exécutez la commande react-native run-ios ou ouvrez le projet directement avec xCode et exécutez le